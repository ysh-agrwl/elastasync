=======================
 README for Elastasync
=======================
:Authors:
  * **Andrew Wilcox**, *lead engineer*
:Organisation:
  Wilcox Technologies, LLC
:Version:
  1.0
:Status:
  Beta
:Copyright:
  Copyright © 2016 Wilcox Technologies LLC.  All rights reserved.

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal with the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject
  to the following conditions:

  Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimers.

  Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimers in
  the documentation and/or other materials provided with the distribution.

  Neither the names of Wilcox Technologies, LLC, nor the names of its
  contributors may be used to endorse or promote products derived from this
  Software without specific prior written permission.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE
  FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
  CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
  WITH THE SOFTWARE.

.. NOTE:: This document contains information about a forthcoming product from
          Wilcox Technologies.  The information contained within may not reflect
          the final product and may change rapidly during development processes.
          Always ensure you have the latest copy of all documents before raising
          any concerns you may have.


Welcome
=======

This repository includes source code for Elastasync, the AsyncIO-based
Elasticsearch module for Python 3.


Supported platforms
===================

As Elastasync is written in pure Python and has only one external dependency,
the only requirement is a computer running Python 3.4.1 or newer.

Python 3.5 is recommended for higher performance (profile-opt).


What's supported?
=================

* The same logging infrastructure as elasticsearch-py.

* Secure TLS (SSL) connections.

* HTTP authentication.

* All exceptions have the same names and base classes as elasticsearch-py.

* The following API methods have the same signatures as elasticsearch-py:

  * ``bulk``

  * ``count``

  * ``get``

  * ``index``

  * ``search``

  but *read the documentation for each method* before using them for important
  differences where applicable.

* **NEW!**  The ability to queue ``index`` operations and automatically ``bulk``
  send them after a predefined number.  See the ``auto_bulk`` and
  ``bulk_queue_size`` keys of the ``Elasticsearch`` class.  This can result in
  great performance boosts for frequent index operations.

* **NEW!** Per-node connection pooling, for parallel operations on the same
  host.  See the ``pool_size`` key of the ``Elasticsearch`` class.


What's not supported (yet)?
===========================

* Sniffing.

* Multiple hosts.

* API methods not listed in the supported section.

If these are important to you, please consider adding support and contributing
it for inclusion in the next version of Elastasync (see Contributing, below).


Contributing
============

If you would like to contribute to Elastasync, please see our contributors
guide, which should be located in the same directory as this README.

We welcome contributions to Elastasync.  Please ensure any new source files you
may add explicitly state they are licensed under the same terms as the rest of
the project so that we may include your contrbutions in the next version of
Elastasync.  Thank you!
