"""Exceptions raised by Elastasync.

The names (and purposes) mostly mimic the official Elasticsearch binding for
source-level compatibility where available.

Copyright © 2016 Wilcox Technologies LLC and contributors.  All rights reserved.
See LICENSE file included with the Elastasync source for more information.
"""


class ImproperlyConfigured(Exception):
    """The configuration provided is invalid."""
    pass


class ElasticsearchException(Exception):
    """Base class for exceptions.  Not raised itself."""
    pass


class SerializationError(ElasticsearchException):
    """A problem occurred while serialising data."""
    pass


class TransportError(ElasticsearchException):
    """Elasticsearch returned an error code or a connection has failed."""

    def __init__(self, code='N/A', msg='An unknown problem has occurred.',
                 info=None):
        """Initialiase the transport error.

        :param str code:
            Either a string version of an HTTP status code, or "N/A" if a
            connection has failed.  Defaults to "N/A".

        :param str msg:
            A simple message suitable for display to the user.

        :param dict info:
            Any information returned by Elasticsearch, if available.
        """

        self.status_code = code
        self.info = info
        self.error = msg


class ConnectionError(TransportError):  # pylint: disable=redefined-builtin
    """The connection has definitely failed now."""
    pass


class ConnectionTimeout(ConnectionError):
    """The specified timeout has expired and no connection was made."""
    pass


class SSLError(ConnectionError):
    """A problem occurred with the underlying TLS mechanism."""
    pass


class NotFoundError(TransportError):
    """The specified record/item was not found."""
    pass


class ConflictError(TransportError):
    """There was a conflict (409) while processing the request."""
    pass


class RequestError(TransportError):
    """The specified request was invalid."""
    pass
