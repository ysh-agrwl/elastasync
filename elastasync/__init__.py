"""Top-level Elastasync module.

This imports the commonly used classes for ease of import (and drop-in
replacement of the original Elasticsearch Python module).

Copyright © 2016 Wilcox Technologies LLC and contributors.  All rights reserved.
See LICENSE file included with the Elastasync source for more information.
"""

from elastasync.elastic import Elasticsearch
