"""The primary module for Elastasync, where the magick happens.

Copyright © 2016 Wilcox Technologies LLC and contributors.  All rights reserved.
See LICENSE file included with the Elastasync source for more information.
"""

import asyncio
from asyncio import coroutine, Lock, TimeoutError
from collections import UserString
from functools import partial
from logging import getLogger
from urllib.parse import urlparse, urlunparse

try:
    import simplejson as json
except ImportError:
    import json

import aiohttp
import aiohttp.client_exceptions

# pylint: disable=redefined-builtin
from elastasync.errors import ConnectionError, ElasticsearchException, \
                              TransportError
# pylint: enable=redefined-builtin


def retry_request(num_retries=3):

    def retry_request_decorator(func):

        def apply_rules(*args, **kwargs):
            for current_retry in range(0, num_retries):
                try:
                    output = yield from func(*args, **kwargs)
                    return output
                except TransportError as ex:
                    if ex.status_code == 503:
                        if current_retry < (num_retries-1):
                            print("Received 503 from ES, retrying num: ", current_retry, " Retrying in 10 seconds")
                            yield from asyncio.sleep(10)
                        else:
                            print("Received 503 from ES, retrying num: ", current_retry, " Quitting")
                            raise ex
                    else:
                        raise ex

        return apply_rules

    return retry_request_decorator


class Elasticsearch:
    """The main Elasticsearch class, AsyncIO-style."""

    def __init__(self, hosts=None, **kwargs):
        """Initialise the Elasticsearch client.

        :param str/list hosts:
            One or more hosts to connect to.  The default is localhost:9200.
            One host may be specified by a simple string; multiple hosts must be
            specified using an iterable.

            .. warning::
                Only one host is currently supported by Elastasync.  Additional
                hosts specified will be *ignored*.

        :key bool use_ssl:
            Determines whether to use TLS (SSL) for secure communication.  This
            is the default if your host specifies an https protocol.

        :key bool verify_certs:
            Determines whether to verify host server certificates when using
            TLS (SSL).  Defaults to False only for compatibility with
            elasticsearch-py; this may change in a future version.

        :key int pool_size:
            Defines the number of simultaneous connections *per host*.  The
            default is one.

        :key bool auto_bulk:
            Determines whether to automatically queue indexing operations for
            later bulk processing.  Defaults to false.  Ensure you read the
            documentation before enabling this feature.

        :key int bulk_queue_size:
            Defines the number of indexing operations to queue before committing
            a bulk index operation.

        :key logger:
            The logger to use for debugging and error logging purposes.  If not
            specified, defaults to creating a new one with the name
            'elastasync.elastic'.

        :key timeout:
            Determines the timeout period to use when performing Elasticsearch
            operations over the network, in seconds.  Defaults to 60.
        """

        if isinstance(hosts, (str, UserString)):
            hosts = [hosts]
        elif hosts is None:
            hosts = ['localhost:9200']

        # We only take the first one for now.
        host_result = urlparse(hosts[0])
        scheme = 'https' if kwargs.get('use_ssl', False) else 'http'
        if host_result.netloc == '':
            host_spec = host_result.path
        else:
            host_spec = host_result.netloc
        if ':' not in host_spec:
            host_spec += ':9200'
        actual_host = urlunparse((scheme, host_spec, '', '', '', ''))
        self.use_host = actual_host

        self.connector = partial(aiohttp.TCPConnector,
                                 verify_ssl=kwargs.get('verify_certs', False),
                                 limit=kwargs.get('pool_size', 1))
        self.session = aiohttp.ClientSession(connector=self.connector())

        self.logger = kwargs.get('logger', getLogger(__name__))
        self.timeout = int(kwargs.get('timeout', 60))

        self.auto_bulk = kwargs.get('auto_bulk', False)
        if self.auto_bulk:
            self.bulk_lock = Lock()
            self.bulk_queue = list()
            self.bulk_queue_watermark_lo = kwargs.get('bulk_queue_size', 1000)
            self.bulk_queue_watermark_hi = self.bulk_queue_watermark_lo * 8

    def _fetch_session(self):
        """Retrieve the current session, ensuring it isn't closed."""

        if self.session.closed:
            self.session = aiohttp.ClientSession(connector=self.connector())

        return self.session

    @retry_request(num_retries=3)
    @coroutine
    def _perform_request(self, req_type, url, body=None, params=None):
        """Perform a request.

        :param str req_type:
            GET, POST, or PUT.

        :param str url:
            The URL of the request.

        :param body:
            The body of the request.

        :param params:
            The parameters of the request.
        """

        # self.logger.debug('Connecting to Elasticsearch at %s for %s',
                        #   url, req_type)
        # print('Connecting to Elasticsearch', url, req_type, body)

        req_type = req_type.lower()
        if req_type not in ('get', 'post', 'put'):
            req_type = 'get'

        call = getattr(self._fetch_session(), req_type)

        # print("Here call variable is: ", call)
        headers = {'content-type': 'application/json'}
        try:
            resp_es = yield from call(url, data=body, params=params,
                                timeout=self.timeout, headers=headers)
        except aiohttp.client_exceptions.ClientOSError as exc:
            self.logger.exception('Cannot connect to Elasticsearch')
            print('Cannot connect to Elasticsearch', exc)
            raise ConnectionError(msg='Cannot connect to ES') from exc
        except TimeoutError as exc:
            self.logger.exception('Timeout connecting to Elasticsearch')
            print("TimeoutError exception: ", ex)
            raise ConnectionError(msg='Cannot connect to ES') from exc
        except Exception as exc:
            self.logger.exception('Unknown exception connecting to Elasticsearch')
            print("Unknown exception: ", exc)
            raise ConnectionError(msg='Cannot connect to ES') from exc

        try:
            output = yield from resp_es.json()
        except ValueError:
            output = None

        yield from resp_es.release()

        if resp_es.status not in range(200, 206) or output is None:
            self.logger.error('Received error %d from Elasticsearch', resp_es.status)
            raise TransportError(code=resp_es.status, info=output)

        # self.logger.debug('Got reply from Elasticsearch: took=%r, errors=%r', output.get('took', 0), output.get('errors', True))

        return output

    @coroutine
    def count(self, index='_all', doc_type=None, **kwargs):
        """Retrieve the number of documents for a query.

        :param str index:
            One or more indexes to search, in comma-separated format.  Defaults
            to _all.

        :param str doc_type:
            One or more document types to search, in comma-separated format.
            Specifying None or an empty string will include all document types.

        :key dict body:
            An optional Query DSL query to use for filtering the results.

        :key int min_score:
            Defines a minimum _score value for inclusion in the count.

        :key str q:
            An optional Lucene query string to use for filtering the results.

        The following additional parameters are supported when a Lucene query
        string is provided:

        :key bool analyze_whitespace:
            Should wildcard and prefix queries be analyzed or not. Defaults to
            false.

        :key str analyzer:
            The analyzer name to be used when analyzing the query string.

        :key str default_operator:
            The default operator to be used, can be AND or OR. Defaults to OR.

        :key str df:
            The default field to use when no field prefix is defined within the
            query.

        :key bool lenient:
            If set to true will cause format based failures (like providing text
            to a numeric field) to be ignored. Defaults to false.

        :key bool lowercase_expended_terms:
            Should terms be automatically lowercased or not. Defaults to true.

        :key int terminate_after:
            The maximum count for each shard, upon reaching which the query
            execution will terminate early. If set, the response will have a
            boolean field terminated_early to indicate whether the query
            execution has actually terminated_early. Defaults to no
            terminate_after.

        The following parameters from Elasticsearch-py are *not* currently
        supported by Elastasync:

        * allow_no_indices (always True)

        * expand_wildcards

        * ignore_unavailable

        * preference

        * routing
        """

        allowed_params = ['min_score', 'q']

        if 'q' in kwargs:
            allowed_params += ['analyze_whitespace', 'analyzer',
                               'default_operator', 'df', 'lenient',
                               'lowercase_expended_terms', 'terminate_after']

        url = self.use_host + '/' + index + '/'
        if doc_type:
            url += doc_type + '/'
        url += '_count'

        params = {key: kwargs.pop(key)
                  for key in allowed_params if key in kwargs}

        body = kwargs.get('body', None)
        if body:
            body = json.dumps(body)

        output = yield from self._perform_request('GET', url, body, params)
        return output

    @coroutine
    def _flush_queue(self):
        """Flushes the bulk queue when it becomes full."""
        supported_params = ('parent', 'routing', 'timestamp', 'ttl', 'version',
                            'version_type')

        request = list()
        oldqueue = list(x for x in self.bulk_queue)

        bulk_count = min(len(self.bulk_queue), self.bulk_queue_watermark_hi)

        for _ in range(0, bulk_count):
            (index, doc_type, doc_id, body, params) = self.bulk_queue.pop()

            op_type = params['op_type']

            operation = {op_type: {'_index': index, '_type': doc_type}}
            if doc_id:
                operation[op_type]['_id'] = doc_id

            addl_params = (key for key in supported_params if key in params)
            for param in addl_params:
                operation[op_type]['_' + param] = params[param]

            request.append(json.dumps(operation))
            request.append(json.dumps(body))

        url = self.use_host + '/_bulk'

        try:
            output = yield from self._perform_request('POST', url,
                                                      str.join('\n', request),
                                                      params)
        except ElasticsearchException:
            self.bulk_queue = oldqueue
            output = None
        else:
            del oldqueue

        return output

    @coroutine
    def index(self, index, doc_type, body, **kwargs):
        """Add a document to the specified index.

        :param str index:
            The index in which to add this document.

        :param str doc_type:
            The type of this document.

        :param dict body:
            The typed JSON document to add.

        The following optional parameters may be specified:

        :key id:
            The ID of the document.

        :key str op_type:
            The type of operation to perform.  Valid options are index and
            create.  The default is index.

        :key str parent:
            The ID of the parent document.

        :key str routing:
            Specifies an explicit routing.

        :key str timeout:
            Specifies the timeout for the primary shard to respond.  Ignored if
            auto_bulk is active.

        :key str timestamp:
            Specifies the timestamp the document should retain.

            .. warning::
                This field has been deprecated in Elasticsearch 2.0.  Don't rely
                on it for new deployments.

        :key str ttl:
            The time-to-live for the document, after which it will be
            automatically removed.

        :key int version:
            Specifies a specific version number for this document.

        :key str version_type:
            Specifies the type of versioning to use for this document.
        """

        allowed_params = ('consistency', 'op_type', 'parent', 'refresh',
                          'routing', 'timeout', 'timestamp', 'ttl', 'version',
                          'version_type')

        if any(x is None for x in (index, doc_type, body)):
            raise ValueError("Index, document, and type must be specified.")

        doc_id = kwargs.pop('id', None)
        params = {key: kwargs.pop(key)
                  for key in allowed_params if key in kwargs}

        if 'op_type' not in params or \
           params['op_type'] not in ('index', 'create'):
            params['op_type'] = 'index'

        if self.auto_bulk:
            yield from self.bulk_lock
            self.bulk_queue.append((index, doc_type, doc_id, body, params))

            if len(self.bulk_queue) % self.bulk_queue_watermark_lo == 0:
                yield from self._flush_queue()
            self.bulk_lock.release()

            return

        url = self.use_host + '/' + index + '/' + doc_type + '/'

        if doc_id:
            url += str(doc_id)
            call = 'PUT'
        else:
            call = 'POST'

        # print("Elastasync: index, going to perform requiest")
        output = yield from self._perform_request(call, url, json.dumps(body),
                                                  params)
        return output

    @coroutine
    def search(self, index=None, doc_type=None, body=None, **kwargs):
        """Perform a query on one or more indexes.

        :param str index:
            One or more indexes to search, in comma-separated format.  Defaults
            to _all.

        :param str doc_type:
            One or more document types to search, in comma-separated format.
            Specifying None or an empty string will include all document types.

        :param dict body:
            Query DSL query to use for the search.

        :key _source:
            If _source is specified as a boolean, determines whether or not to
            return the _source object in the response at all.  If _source is
            specified as a string, provides a list of _source keys to return in
            comma-separated format.

        :key _source_include:
            One or more fields to include in _source, in comma-separated format.
            Wildcards (? and *) are accepted.

        :key _source_exclude:
            One or more fields to exclude from _source, in comma-separated
            format.  Wildcards (? and *) are accepted.

        :key str analyzer:
            The analyzer name to be used when analyzing the query string.

        :key bool analyze_wildcard:
            Determines if wildcard and prefix queries should be analysed.  The
            default is False.

        :key str default_operator:
            The default operator to be used for query parameters.  Valid options
            are AND, and OR.  Defaults to OR.

        :key str df:
            The default field to use when no field prefix is defined within the
            query.

        :key bool explain:
            If True, return an explanation of how scoring each hit was computed.
            Defaults to False.

        :key str fields:
            Determines which selective stored fields to provide with each result
            in comma-separated format.  The default is none.

        :key bool ignore_unavailable:
            If True, ignore indicies that are missing or otherwise unavailable.
            The defaults is False.

        :key bool lenient:
            If set to True will cause format based failures (like providing text
            to a numeric field) to be ignored. The default is False.

        :key bool lowercase_expended_terms:
            Determines if terms should automatically be lowercased or not.
            The default is True.

        :key str q:
            An optional Lucene query string to use for the searc.

        :key str search_type:
            The type of search operation to perform.  The default is
            query_then_fetch.

        :key int size:
            Determines the number of results to return.  The default is 10.

        :key str sort:
            Specifies the sorting to perform.

        :key int terminate_after:
            The maximum count for each shard, upon reaching which the query
            execution will terminate early. If set, the response will have a
            boolean field terminated_early to indicate whether the query
            execution has actually terminated_early. Defaults to no
            terminate_after.

        :key int timeout:
            If specified, the request will terminate after the specified
            timeout period, returning the results accumulated thus far.  The
            default is to not have a timeout period.

        :key bool version:
            Determines whether to return the document version number for each
            result.  The default is False.
        """

        allowed_params = ('_source', '_source_include', '_source_exclude',
                          'analyzer', 'analyze_wildcard', 'default_operator',
                          'df', 'explain', 'fields', 'ignore_unavailable',
                          'lenient', 'lowercase_expended_terms', 'q',
                          'search_type', 'size', 'sort', 'terminate_after',
                          'timeout', 'version')

        if not index:
            index = '_all'

        url = self.use_host + '/' + index + '/'
        if doc_type:
            url += doc_type + '/'
        url += '_search'

        params = {key: kwargs.pop(key)
                  for key in allowed_params if key in kwargs}

        # lenient is, hilariously, lenient.  it happily accepts capital bools.
        for bool_param in ('_source', 'analyze_wildcard', 'explain', 'version'):
            if bool_param in params and isinstance(params[bool_param], bool):
                params[bool_param] = str(params[bool_param]).lower()

        if body:
            body = json.dumps(body)

        output = yield from self._perform_request('GET', url, body, params)
        return output

    @coroutine
    def close(self):
        """Clean up resources."""

        yield from self.session.close()
