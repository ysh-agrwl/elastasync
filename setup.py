#!/usr/bin/env python3

# Copyright © 2016 Wilcox Technologies LLC and contributors.
# All rights reserved.
# See LICENSE file included with the Elastasync source for more information.


from setuptools import setup

classifiers = (
    'Development Status :: 4 - Beta',
    'Intended Audience :: Developers',
    'Intended Audience :: Information Technology',
    'License :: OSI Approved :: University of Illinois/NCSA Open Source '
    'License',
    'Operating System :: OS Independent',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3 :: Only',
    'Topic :: Database :: Front-Ends',
    'Topic :: Internet :: Log Analysis',
    'Topic :: Scientific/Engineering :: Information Analysis'
)

setup(name='elastasync', description='AsyncIO-based Elasticsearch module',
      author='Wilcox Technologies LLC', author_email='oss@wilcox-tech.com',
      url='http://oss.wilcox-tech.com', license='NCSA',
      packages=['elastasync'],
      install_requires=['aiohttp >= 0.20.0'],
      version='0.1',
      keywords=('elasticsearch', 'asyncio', 'big data'),
      classifiers=classifiers)
