====================================
 Contributor's Guide for Elastasync
====================================
:Authors:
  * **Andrew Wilcox**, *lead engineer*
:Organisation:
  Wilcox Technologies, LLC
:Version:
  1.0
:Status:
  Beta


Welcome
=======

Thank you for your interest in contributing to Elastasync!  We value our
community and your time, and as such this guide is written to be easy to follow.


Licensing
=========

To protect the rights of our users, all code contributed to Elastasync must have
a clear license header that states the contribution is licensed under the same
terms as Elastasync.  If you are making a change to one or more files that
already exist in the source tree, the license header already in place in those
files provides an implicit grant for your code to be additionally licensed under
the same terms.  Please ensure that you have the necessary permission from any
employer(s) to contribute code you write under the terms of the NCSA license.


Code syntax
===========

* Indentation must be four spaces.  *Never* use hard tabs (\t).

* Line length is hard 80 characters.  There are *no* exceptions.  Python allows
  continuations using the ``\`` character.  It is preferable to have a single
  space before the ``\`` character.

* Code, in general, must follow the guidelines set forth in PEP 8.

* The tox.ini file in the root of the project specifies pep8 settings.  We
  humbly request that you run ``pep8 .`` from the top-level to ensure that your
  contributions do not cause any style issues.  Failure to do so may cause a
  delay in accepting your contribution until the issues introduced are fixed.


Documentation
=============

* All modules, classes, and methods *must* be documented.  Parameters *should*
  be documented.  Remember that names may be obvious to you right now, but may
  mean something else entirely to someone else - or even you during maintenance.

* Publicly accessible instance variables *must* be documented in the class
  docstring, using the ``:ivar name:  Description`` format.
